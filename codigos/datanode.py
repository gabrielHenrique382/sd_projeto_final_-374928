import os,sys
from xmlrpc.server import SimpleXMLRPCServer		

chave_de_separar = "!@$!@#!@"
chave_de_arquivo_temp = "#@!#@&@"

try:
    my_ip = input('Digite o ip do datanode: ') #IP do server
    my_port = (input('Digite a porta do datanode: ')) #Porta de comunicacao
    my_port = int(my_port)
    server = SimpleXMLRPCServer((my_ip, my_port))		#Chama o servidor
except:
    print("Servidor invalido!")
    print("Saindo do programa.")
    sys.exit(0)

name_of_path = os.path.dirname(sys.argv[0])
directory_name = 'datanode(' + my_ip + ','+ str(my_port) +')'
if not os.path.exists(directory_name):
   os.makedirs(directory_name)

# Pegar lista atual da maquina
def get_list_of_files():
    completeName = os.path.join(name_of_path + '/' + directory_name, 'lista.txt')
    arq = open(completeName,'a+')    #Abre arquivo lista de arquivos presente no computador
    arq.seek(0)
    texto = str(arq.read())
    A = texto.split("!@$!@#!@")     #Usa a chave de separação para definir o nome dos arquivos já armazenados 
    A = A[1:]                       #Recupera o vetor de palavras
    arq.close()
    return A

lista = get_list_of_files()         

# Enviar pro servidor proxy lista inicial de arquivos
def ask_for_list_of_files():
    completeName = os.path.join(name_of_path + '/' + directory_name, 'lista.txt')
    arq = open(completeName,'a+')
    arq.seek(0)
    texto = str(arq.read())
    arq.close()
    return (texto, chave_de_separar, my_ip,my_port)

def create_temp(name,y):
    message = read_file(name)
    newName = chave_de_arquivo_temp + name
    completeName = os.path.join(name_of_path + '/' + directory_name, newName)
    arq = open(completeName,'w')
    arq.write(message)
    arq.close()
    if(y == 1):
        completeName = os.path.join(name_of_path + '/' + directory_name, 'lista_temp.txt')        
        arq = open(completeName,'a+')     #Adiciona a lista.txt
        arq.write(chave_de_separar + newName)
        arq.close()
    return

def recover_from_temp(name):
    completeName = os.path.join(name_of_path + '/' + directory_name, 'lista_temp.txt')
    arq = open(completeName,'a+')
    arq.seek(0)
    texto = str(arq.read())
    arq.close()
    m = texto.split(chave_de_separar) #Elimina a chave de forma a obter o nome dos arquivos separadamente
    m = m[1:]
    for i in range(len(m)):
        if(m[i][len(chave_de_arquivo_temp):] == name):
            newName = (chave_de_arquivo_temp + name)
            completeName = os.path.join(name_of_path + '/' + directory_name, newName)
            arq = open(completeName,'a+')
            arq.seek(0)
            msg = str(arq.read())
            arq.close()
            write_file(name,msg,3)
            return "R"
    return "NR"
    


#Escreve determinada mensagem em um determinado arquivo 
#1 -> Escreve por cima]
#2 -> Escreve no fim
#3 -> Recupera o arquivo do temp
def write_file(name,message,y):
    completeName = os.path.join(name_of_path + '/' + directory_name, name)
    if(name in lista):                  #Verifica se o arquivo está na lista de arquivos já existentes
        if(y == 1):                     #Caso y seja 1, a mensagem sobrescreverá o que já está no arquivo
            create_temp(name,2)
            arq = open(completeName,'w')
            arq.write(message)
            arq.close()
        elif(y == 2):                           #Caso contrário, a mensagem será adicionada no fim do que já está no arquivo
            create_temp(name,2)
            arq = open(completeName, 'a')
            arq.write(message)
            arq.close()
        else:
            arq = open(completeName, 'w')
            arq.write(message)
            arq.close()
        return 'Mensagem escrita'       
    else:                               #Caso o arquivo não esteja na lista, deve-se criá-lo
        arq = open(completeName,'w')            #Cria e modifica o arquivo
        arq.write(message)
        arq.close()
        completeName = os.path.join(name_of_path + '/' + directory_name, 'lista.txt')        
        arq = open(completeName,'a')     #Adiciona a lista.txt
        arq.write(chave_de_separar + name)
        arq.close()
        lista.extend([name])            #Adiciona ao array lista
        create_temp(name,1)
        return 'arquivo criado e modificado' 

def read_file(name):                    #Função para ler o arquivo
	if(name in lista):                  #Verifica se o arquivo está presente na lista
		completeName = os.path.join(name_of_path + '/' + directory_name, name)
		arq = open(completeName,'r')
		texto = arq.read()
		arq.close()
		return texto
	else:                               #Caso contrário, retorna mensagem de erro
		return 'Arquivo nao encontrado'

def remove_file(name):                  #Função para remover o arquivo da lista
	if(name in lista):                  #Verifica se o arquivo está presente na lista
		create_temp(name,2)
		completeName = os.path.join(name_of_path + '/' + directory_name, name)
		os.remove(completeName)                 #Remove arquivo fisico
		lista.remove(name)		        #Remove do Array lista
		
		completeName = os.path.join(name_of_path + '/' + directory_name, 'lista.txt')
		arq = open(completeName,'w')
		arq.write("")                   #Remove do Array lista
		arq.close()
		
		arq = open(completeName,'a')	    #Adicionar todos os arquivos atuais
		for i in range(len(lista)):
			arq.write(chave_de_separar + lista[i])
		arq.close()
		
		return 'arquivo removido'
	else:                               #Caso contrário, retorna mensagem de erro
		return 'nome incorreto ou arquivo nao existe'

def remove_file_from_temp(name):                  #Função para remover o arquivo da lista
    completeName = os.path.join(name_of_path + '/' + directory_name, 'lista_temp.txt')
    arq = open(completeName,'a+')
    arq.seek(0)
    texto = str(arq.read())
    arq.close()
    m = texto.split(chave_de_separar) #Elimina a chave de forma a obter o nome dos arquivos separadamente
    m = m[1:]
    novaLista = ""
    existe_arquivo = False
    for i in range(len(m)):
        if(m[i][len(chave_de_arquivo_temp):] == name):
            newName = chave_de_arquivo_temp + name
            completeName = os.path.join(name_of_path + '/' + directory_name, newName)
            os.remove(completeName)                 #Remove arquivo fisico
            existe_arquivo = True
        else:
            novaLista += chave_de_separar + m[i]
    if(existe_arquivo):
        completeName = os.path.join(name_of_path + '/' + directory_name, 'lista_temp.txt')
        arq = open(completeName,'w')
        arq.write(novaLista)                   #Remove do Array lista
        arq.close()
        return "DELETADO"
    else:
        return "NAO_EXISTIA"
				

#Declaracao das funcoes implementadas
server.register_function(recover_from_temp, "recover_from_temp")
server.register_function(remove_file_from_temp, "remove_file_from_temp")
server.register_function(ask_for_list_of_files, "ask_for_list_of_files")	
server.register_function(write_file, "write_file")
server.register_function(read_file, "read_file")
server.register_function(remove_file, "remove_file")
server.serve_forever()
