from xmlrpc.server import SimpleXMLRPCServer        
from xmlrpc.server import SimpleXMLRPCRequestHandler
import xmlrpc.client
import hashlib
import json
import sys

try:
    my_ip = input('Digite o ip do namenode: ') #IP do server
    my_port = (input('Digite a porta do namenode: ')) #Porta de comunicacao
    my_port = int(my_port)
    server = SimpleXMLRPCServer((my_ip, my_port))		#Chama o servidor
except:
    print("Servidor invalido!")
    print("Saindo do programa.")
    sys.exit(0)

chave = "!@#$"      #String usada para separação dos arquivos na lista

table = dict([('sape',("4139",))])  #Inicialização da estrutura dicionário
del table['sape']

number_of_datanodes = 0

def register_all_services(lista, chave, datanode_n,numberOfDatanodes): #Função para registrar arquivos
    global number_of_datanodes
    number_of_datanodes = numberOfDatanodes
    m = lista.split(chave) #Elimina a chave de forma a obter o nome dos arquivos separadamente
    m = m[1:]
    for i in range(len(m)):
        register_service(datanode_n,m[i])  #Registra os arquivos
    print(str(table))
    return '-Operacao realizada com sucesso'

def register_service(datanode_n, datanode_file): #Função para registrar determinado arquivo com o ip de seu datanode
    if datanode_file in table:                    #Verifica se o  arquivo já foi registrado
        if(datanode_n in table[datanode_file]):  #Verifica se o datanode já está registrado
            return "arquivo ja registrado"
        else:                                     #Caso contrário, registra o ip adicional
            table[datanode_file] += (datanode_n,)
    else:                                         #Caso o arquivo não esteja no datanode, este será criado
        table[datanode_file] = (datanode_n,)
    print (str(table))
    return "arquivo registrado"

def combinacoesPossiveis(n):
    A = []
    for i in range(n):
        for j in range(i + 1,n):
            if(i != j):
                A.append((i,j));
    return A;

def register_service_for_the_first_time(datanode_file):
    A =  combinacoesPossiveis(number_of_datanodes)
    print(str(A))
    data_md5 = hashlib.md5(json.dumps(datanode_file, sort_keys=True).encode('utf-8')).hexdigest()
    if(number_of_datanodes == 0):
        return "Nao ha servidores disponiveis"
    elif(number_of_datanodes == 1):
        table[datanode_file] = ('0',)
        return (0,)
    else:
        datan = int(data_md5, 16)%len(A);
        print(datan)
        table[datanode_file] = (str(A[datan][0]),str(A[datan][1]))
        return (A[datan][0],A[datan][1])
    

def get_datanodes_of_service(name):                   #Função para recuperar os numeros dos datanodes dos arquivos bem como a chave de separação
    if name in table:                           #Verifica se o arquivo já está registrado
        lista=""
        for i in range(len(table[name])):
            lista += chave + table[name][i]     #Adiciona à uma  lista vazia a chave e a estrutura dicionário do arquivo requerido
        return (lista,chave)
    else:                                       #Caso contrário, retorna mensagem de erro
        return "Nao existe tal arquivo"

def remove_service(datanode_n, datanode_file): #Função para remover arquivo
    if datanode_file in table:                  #Verifica se o dicionário possui o arquivo
        if(datanode_n in table[datanode_file]):#Verifica se o arquivo está registrado para o datanode
            table[datanode_file] = tuple(x for x in table[datanode_file] if (x != datanode_n))
            if(len(table[datanode_file]) <= 0):
                del table[datanode_file]        #Exclui o arquivo do dicionário
            return "arquivo removido"
        else:                                   #Verifica que o datanode não possui o arquivo requerido
            return "Esse datanode nao possui tal arquivo"
    else:                                       #Verifica que o arquivo não consta na estrutura dicionário
        return "Nao existe esse arquivo"
    
def remove_service_for_all_datanodes(name_file):    #Remove o arquivo para todos os datanodes
    if name_file in table:
        del table[name_file]
        return "arquivo removido"
    else:
        return "Nao existe esse arquivo"    
    
def get_list_of_files():            #Retorna lista de arquivos 
    lista = str(table.keys());
    return lista[11:len(lista)-2]


#Declaracao das funcoes implementadas
server.register_function(register_service_for_the_first_time, "register_service_for_the_first_time")
server.register_function(remove_service, "remove_service")
server.register_function(remove_service_for_all_datanodes, "remove_service_for_all_datanodes")
server.register_function(register_service, "register_service")
server.register_function(get_datanodes_of_service, "get_datanodes_of_service")
server.register_function(register_all_services, "register_all_services")
server.register_function(get_list_of_files, "get_list_of_files")
server.serve_forever()   
