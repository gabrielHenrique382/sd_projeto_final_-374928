import xmlrpc.client

ip = input('Digite o ip do server: ')				#Solicita o IP do server que o cliente deseja se conectar
port = input('Digite a porta da aplicacao: ')		#Solicita a porta para a comunicacao
try:
    proxy = xmlrpc.client.ServerProxy("http://"+ip+":"+ port +"/") 	#Estabelece a conexao
except:
    print("Não foi possível se conectar com o servidor.")
while True:

    print("Digite 1 para escrever")	#Escrever sobre, anexar no fim ou criar o arquivo
    print("Digite 2 para ler")      #Ler arquivos
    print("Digite 3 para remover")  #Remover arquivos
    print("Digite 4 para ver todos os arquivos") #Mostra uma lista de arquivos
    print("Digite 5 para sair") 
	
    try:
        stx = input('Qual numero: ')
        x = int(stx)				#Armazena a opcao do usuario e as designa para as funcoes corretas
    except:
        print("Comando não aceito, tente Novamente.\n")
        continue
    if(x == 1):
        print("\nDigite 1 para escrever por cima")
        print("Digite 2 para anexar no fim")
        try:
            sty = input('Qual numero: ')
            y = int(sty)                 #Armazena a opção de escrita desejada
        except:
            print("Comando não aceito, tente Novamente.\n")
            continue
        if(y == 1 or y == 2):
            fileName = input('Qual o nome do arquivo: ')
            if(len(fileName) < 4):
                fileName += ".txt"
            else:
                if(fileName[len(fileName)-4 : len(fileName)] != ".txt"):
                    fileName += ".txt"
            message = input('Qual a mensagem: ')
            if(fileName == "lista.txt" or fileName == "lista_temp.txt"):
                print("Nome já reservado")
            else:
                print(proxy.write_file(fileName, message, y))	#Chama a funcao de escrever a mensagem
        else:
            print("Comando não aceito, tente Novamente.\n")
            continue
    elif(x == 2):
        fileName = input('Qual o nome do arquivo: ')
        if(len(fileName) < 4):
            fileName += ".txt"
        else:
            if(fileName[len(fileName)-4 : len(fileName)] != ".txt"):
                fileName += ".txt"
        if(fileName == "lista.txt" or fileName == "lista_temp.txt"):
            print("Nome já reservado")
        else:
            print(proxy.read_file(fileName))			#Chama a funcao para ler as mensagens    	
    elif(x == 3):
        fileName = input('Qual o nome do arquivo: ')
        if(len(fileName) < 4):
            fileName += ".txt"
        else:
            if(fileName[len(fileName)-4 : len(fileName)] != ".txt"):
                fileName += ".txt"
        if(fileName == "lista.txt" or fileName == "lista_temp.txt"):
            print("Nome já reservado")
        else:
            print(proxy.remove_file(fileName))			#Chama a funcao para remover as mensagens
		
    elif(x == 4):
        print("\n" + proxy.get_list_of_files() + "\n")			#Chama a funcao para ler as mensagens
		
    elif(x == 5):
        break
    else:
        print("Comando não aceito, tente Novamente.")
    print("\n")
        
