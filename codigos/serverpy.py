from xmlrpc.server import SimpleXMLRPCServer		
from xmlrpc.server import SimpleXMLRPCRequestHandler
from datetime import datetime
import xmlrpc.client
import sys

datanode = []		#Cria lista que armazenara os ips dos datanodes
datanodes_proxy = []

#192.168.25.9
try:
    my_ip = input('Digite o ip do servidor: ') #IP do server
    my_port = (input('Digite a porta do servidor: ')) #Porta de comunicacao
    my_port = int(my_port)
    server = SimpleXMLRPCServer((my_ip, my_port))		#Chama o servidor
except:
    print("Servidor invalido!")
    print("Saindo do programa.")
    sys.exit(0)
    
def logAndPrint(message):       #Função que adiciona registro no log e mostra na tela do usuário
	print(message)
	arq = open('log.txt','a')
	arq.write(message + "\n")
	arq.close()


while(1):
    try:
        nip = input('Digite o ip do namenode: ')
        nport = input('Digite a porta do namenode: ')
        namenode = (nip,nport)
        namenode_proxy = xmlrpc.client.ServerProxy("http://"+namenode[0]+":"+ namenode[1] +"/")
        break
    except:
        print("Namenode invalido!")

try:
    number_of_datanodes = input('Quantos datanodes serão utilizados: ')
    number_of_datanodes = int(number_of_datanodes)
except:
    print("Numero invalido!")
    sys.exit(0)
    
if(number_of_datanodes <= 0):
    print("Datanodes invalido!")
    print("Saindo do programa.")
    sys.exit(0)
else:
    i = 0;
    logAndPrint("\n-Iniciando servidores em: " + str(datetime.now().date())+" - " + str(datetime.now().hour)+":"+str(datetime.now().minute)+":"+str(datetime.now().second))
    while(i < number_of_datanodes):
        ip = input('Digite o ip do datanode: ')				#Solicita o IP do datanode
        port = input('Digite a porta do datanode: ')		#Solicita a porta para a comunicacao
        try:
            d_proxy = xmlrpc.client.ServerProxy("http://"+ip+":"+ port +"/")
            logAndPrint("-Iniciando servidor: (" + ip + ", " + port + ")")
            i += 1
            datanodes_proxy.append(d_proxy)
            datanode.append((ip,port))
        except:
            print("Datanode invalido.")

def get_number_of_datanode(datanode_ip,datanode_port):
	n = -1
	for i in range(0,len(datanode)):
		if(datanode[i][0] == datanode_ip and datanode[i][1] == datanode_port):
			n = i
			break
	return n

def start_server():									#Funcao para iniciar os componentes do sistema
	for i in range(0, len(datanodes_proxy)):
		logAndPrint("-Solicitando lista do servidor datanode "+ str(i) + ": (" + datanode[i][0] + ", " + datanode[i][1] + ")")
		(lista, chave_de_separar, datanode_ip,datanode_port) = datanodes_proxy[i].ask_for_list_of_files()     #Recupera os arquivos do datanode 
		print(lista.replace(chave_de_separar, "  "))
		logAndPrint("-Registrando no namenode")
		datanode_n = get_number_of_datanode(datanode_ip,str(datanode_port))
		print(datanode_ip + " " + str(datanode_port) + " " + str(datanode_n ))
		print(namenode_proxy.register_all_services(lista,chave_de_separar,str(datanode_n),number_of_datanodes))#Registrando os arquivos recuperados no namenode

start_server()

def proxys_of_ns(datanode_ns):
	A = []
	for datanode_n in datanode_ns:
		for i in range(len(datanode)):
			if(i == int(datanode_n)):
				A.append(datanodes_proxy[i])
				break
	if(len(A) > 0):
		return A   
	return "Null"

def read_file(name):    #Função para ler determinado arquivo
    logAndPrint("- Solicitacao de leitura do arquivo \"" +name + "\"")
    listaEChave = namenode_proxy.get_datanodes_of_service(name)   #Recupera o arquivo, com seus numeros e sua chave
    if(str(listaEChave) == ("Nao existe tal arquivo")):     #Caso o arquivo não exista no namenode
        return listaEChave
    else:   #Caso contrário
        all_datanodes_ns = listaEChave[0].split(listaEChave[1])    #Separa a chave do arquivo
        all_datanodes_ns = all_datanodes_ns[1:]                   #Recupera os numeros que possuem o arquivo
        dproxys = proxys_of_ns(all_datanodes_ns)
        i = 0
        while(i < len(dproxys)):
            try:
                dproxy = dproxys[i]                                         #Define o ip escolhido na variável dproxy
                message = dproxy.read_file(name)
                logAndPrint("- Arquivo encontrado no datanode \"" + all_datanodes_ns[i] + "\"")
                return message                                              #Retorna a mensagem a ser lida
            except:
                i += 1
        return "Não foi possível recuperar o arquivo"

def write_in_one_datanode(i,name,message):                        #Função para escrever em um datanode apenas
	dproxy = datanodes_proxy[i]        
	datanode_n = str(get_number_of_datanode(datanode[i][0],datanode[i][1]))                           #Define o n do datanode escolhido
	logAndPrint("- Arquivo criado no datanode \"" + str(datanode_n) + "\"")
	namenode_proxy.register_service(datanode_n,name)            #Adiciona o arquivo 
	return dproxy.write_file(name,message,1)                        #Escreve no arquivo 

def write_file(name,message,y):                                     #Função para escrever em arquivo 
    logAndPrint("- Solicitacao de escrita do arquivo \"" +name + "\"")
    listaEChave = namenode_proxy.get_datanodes_of_service(name)           #Recupera o arquivo, com seus ip's, e sua chave
    
    if(str(listaEChave) == ("Nao existe tal arquivo")):             #Caso o arquivo não exista no namenode, este será criado
        tupla_de_servidores = namenode_proxy.register_service_for_the_first_time(name)
        if(str(tupla_de_servidores) == "Nao ha servidores disponiveis"):
            return tupla_de_servidores
        else:
            escreveu_em_todos = True
            for i in range(len(tupla_de_servidores)):            
                try:
                    write_in_one_datanode(tupla_de_servidores[i],name,message)
                except:
                    logAndPrint("- Nao foi possivel escrever no datanode \"" + str(tupla_de_servidores[i]) + "\"")
                    escreveu_em_todos = False
            if(escreveu_em_todos):
                logAndPrint("- Escrita bem sucedida")
                return "Arquivo criado"
            else:
                namenode_proxy.remove_service_for_all_datanodes(name)
                for i in range(len(tupla_de_servidores)): 
                    try:
                        dproxy = datanodes_proxy[tupla_de_servidores[i]]
                        dproxy.remove_file(name)
                        dproxy.remove_file_from_temp(name)
                    except:
                        print("Nao foi possivel remover um arquivo")
                logAndPrint("- Nao foi possível escrever o arquivo \"" +name + "\"")
                return "Nao foi possível escrever o arquivo" 
            
    else:
        all_datanodes_ns = listaEChave[0].split(listaEChave[1])    #Separa a chave dos ips do datanode
        all_datanodes_ns = all_datanodes_ns[1:]                   #Cria o vetor de ips
        dproxys = proxys_of_ns(all_datanodes_ns)                  #Procura o arquivo no datanode
        if(str(dproxys) == "Null"):                                 #Caso não tenha sido encontrado
            return "Arquivo nao encontrado"    
        else:                                                       #Caso tenha sido encontrado
            escreveu_em_todos = True
            for i in range(0, len(dproxys)):
                try:
                    dproxys[i].write_file(name,message,y)               #Escreve a mensagem no arquivo
                    logAndPrint("- Arquivo escrito no datanode \"" + all_datanodes_ns[i] + "\"")
                except:
                    escreveu_em_todos = False
            if(escreveu_em_todos):
                logAndPrint("- Escrita bem sucedida")
                return "Escrita bem sucedida"
            else:
                logAndPrint("- Problemas ao escrever arquivo \"" +name + "\"")
                for i in range(0, len(dproxys)):
                    try:
                        dproxys[i].recover_from_temp(name)               #Escreve a mensagem no arquivo
                    except:
                        print("Nao foi possível recuperar um arquivo temporario")
                logAndPrint("- Nao foi possível escrever o arquivo \"" +name + "\"")
                return "Nao foi possível escrever o arquivo"
            
            
def remove_file(name):                                              #Função para remover um arquivo
    logAndPrint("- Solicitacao de remocao do arquivo \"" +name + "\"")
    listaEChave = namenode_proxy.get_datanodes_of_service(name)            #Recupera o arquivo, com seus ip's, e sua chave
    if(str(listaEChave) == ("Nao existe tal arquivo")):              #Caso o arquivo não exista no namenode, este será criado
        logAndPrint("- Nao existe tal arquivo")
        return "Nao existe tal arquivo"
    else:
        all_datanodes_ns = listaEChave[0].split(listaEChave[1])
        all_datanodes_ns = all_datanodes_ns[1:]
        dproxys = proxys_of_ns(all_datanodes_ns)
        if(str(dproxys) == "Null"):
            logAndPrint("- Arquivo nao encontrado")
            return "Arquivo nao encontrado"    
        else:
            removeu_todos = True
            for dproxy in dproxys:
                try:
                    dproxy.remove_file(name)                            #Remove o arquivo da lista  
                except:
                    removeu_todos = False
                    break
            if(removeu_todos):
                for dproxy in dproxys:
                    try:
                        dproxy.remove_file_from_temp(name)                            #Remove o arquivo da lista  
                    except:
                        print("Nao foi possível remover um arquivo temporario")
                namenode_proxy.remove_service_for_all_datanodes(name)   #Remove o arquivos dos datanodes em que ele está armazenado
                logAndPrint("- Arquivo removido com sucesso")
                return 'arquivo removido'
            else:
                for dproxy in dproxys:
                    try:
                        dproxy.recover_from_temp(name)                            #Remove o arquivo da lista  
                    except:
                        print("Nao foi possível recuperar um arquivo")
                logAndPrint("- Nao foi possível remover o arquivo \"" +name + "\"")
                return "Nao foi possível remover o arquivo"

def get_list_of_files():                                            #Retorna a lista de arquivos registados no namenode
	logAndPrint("- Solicitacao de ver lista de arquivos")
	lista = namenode_proxy.get_list_of_files() 
	return lista			

#Declaracao das funcoes implementadas
server.register_function(get_list_of_files, "get_list_of_files")	
server.register_function(write_file, "write_file")
server.register_function(read_file, "read_file")
server.register_function(remove_file, "remove_file")
server.serve_forever()		
